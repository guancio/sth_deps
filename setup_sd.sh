echo "change DISK with the right sd device"
export DISK=/dev/null

#Installs the bootloaders. Sets position at seek*bs=1*128k and writes count*bs=1*128k. bs is block size for both input and output files.
sudo dd if=./MLO of=${DISK} count=1 seek=1 bs=128k
sudo dd if=./u-boot.img of=${DISK} count=2 seek=1 bs=384k

